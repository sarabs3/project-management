import React, { Component } from 'react';

const names = [
  'Oliver Hansen',
  'Van Henry',
  'April Tucker',
  'Ralph Hubbard',
  'Omar Alexander',
  'Carlos Abbott',
  'Miriam Wagner',
  'Bradley Wilkerson',
  'Virginia Andrews',
  'Kelly Snyder',
];

class UsersList extends Component {
  state = {
    name: [],
    selected: true
  }
  handleChange = event => {
    this.setState({ name: event.target.value, selected: false });
    this.props.selectedUser(event.target.value);
  };

  render() {
    const { selected } = this.state
    return (
      <div className='taskList__assign'>
          {selected ? <select
            value={this.state.name}
            onChange={this.handleChange}
            input={<input id="select-multiple-checkbox" />}
          >
            {names.map(name => (
              <option key={name} value={name}>
                <img className='image' alt="Remy Sharp" src="https://material-ui.com/static/images/remy.jpg" />
                <span>{name}</span>
              </option>
            ))}
          </select>: <img className='image' alt="Remy Sharp" src="https://material-ui.com/static/images/remy.jpg" />}
      </div>
    );
  }
}

export default UsersList;
