import React from 'react';
import UsersList from './usersList';

class TaskList extends React.Component {
    state = {
        tasks: [
            {title: "Task 1", deadline:"Monday"},
            {title: "Deliever the project", deadline:"Friday"}
        ],
        addTask: false,
        taskValue: ''
    }
    handleInput = (key, value) => {
        this.setState(oldState => ({[key]:value}))
    }
    addTask = () => {
        let tasks = this.state.tasks;
        tasks.push(({title:this.state.taskValue, deadline: new Date().getDate()}));
        this.setState(oldState => ({tasks, addTask: false, taskValue: ''}));
    }
    handleAdd = () => {
        this.setState(oldState => ({addTask: true}))
    }
    removeTask = (name) => {
        this.setState({
            tasks: this.state.tasks.filter(task => task.title !== name)
        })
    }
    selectedUser = (user) => {
        console.log('user', user)
    }
    render () {
        return (
            <div className="page">
                <h1>Tasks</h1>
                <ul className="taskList">
                    <li>
                        <div className="taskList__list taskList__Header">
                            <div className="taskList__title">Title</div>
                            <div className="taskList__assign">
                                User
                            </div>
                            <div className="taskList__deadline">Deadline</div>
                        </div>
                    </li>
                    
                    {this.state.addTask && <li>
                        <div className="taskList__list taskList__Add-task">
                            <div className="taskList__title">
                                <form onSubmit={this.addTask}>
                                    <input placeholder="Task" value={this.state.taskValue} onChange={e => this.handleInput('taskValue', e.target.value)} />
                                </form>
                            </div>
                            <UsersList selectedUser={this.selectedUser} />
                            <div className="taskList__deadline">
                                <div className="data">Staticday</div>
                                <div className="actions">
                                    <span>+</span>
                                    <span onClick={() => this.removeTask()}>-</span>
                                </div>
                            </div>
                        </div>
                    </li>}
                    {this.state.tasks.map((task, index) =>
                        <li key={task.title}>
                            <div className="taskList__list">
                                <div className="taskList__title">{task.title}</div>
                                <div className="taskList__assign">
                                    <img
                                        alt="Adelle Charles"
                                        src="https://material-ui.com/static/images/uxceo-128.jpg"
                                        className='image'
                                    />
                                    {/* https://material-ui.com/static/images/uxceo-128.jpg */}
                                </div>
                                <div className="taskList__deadline">
                                    <div className="data">{task.deadline}</div>
                                    <div className="actions">
                                        <span>+</span>
                                        <span onClick={() => this.removeTask(task.title)}>-</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    )}
                    <li>
                        <div className="taskList__list taskList__list--button">
                            <button onClick={this.handleAdd} className="taskList__add-button">Add New task</button>
                        </div>
                    </li>
                </ul>
            </div>
        )
    }
};

export default TaskList;